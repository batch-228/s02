console.log("Hello World!");

//without the use of objects, our students from before would be organized as follows if we are to record additional information about them

//create student one
// let studentOneName = 'John';
// let studentOneEmail = 'john@mail.com';
// let studentOneGrades = [89, 84, 78, 88];

// //create student two
// let studentTwoName = 'Joe';
// let studentTwoEmail = 'joe@mail.com';
// let studentTwoGrades = [78, 82, 79, 85];

// //create student three
// let studentThreeName = 'Jane';
// let studentThreeEmail = 'jane@mail.com';
// let studentThreeGrades = [87, 89, 91, 93];

// //create student four
// let studentFourName = 'Jessie';
// let studentFourEmail = 'jessie@mail.com';
// let studentFourGrades = [91, 89, 92, 93];

// //actions that students may perform will be lumped together
// function login(email){
//     console.log(`${email} has logged in`);
// }

// function logout(email){
//     console.log(`${email} has logged out`);
// }

// function listGrades(grades){
//     grades.forEach(grade => {
//         console.log(grade);
//     })
// }

//This way of organizing employees is not well organized at all.
//This will become unmanageable when we add more employees or functions
//To remedy this, we will create objects


let studentOne = {
	name:"John",
	email:"John@mail.com",
	grades:[ 89, 84, 78, 88],
	logIn(){
    console.log(`${this.email} has logged in`)
	},    
	logOut(){
    console.log(`${this.email} has logged out`)
},
    listGrades(){
    this.grades.forEach( (grade) => {
        console.log(grade);
    })
	}
 }



console.log(`studentOne one's name is ${studentOne.name}`);
console.log(`studentOne one's email is ${studentOne.email}`);
console.log(`studentOne one's grades is ${studentOne.grades}`);

console.log(studentOne.logIn());
console.log(studentOne.logOut());
console.log(studentOne.listGrades());


let studentTwo = {
	name:"Joe",
	email:"Joe@mail.com",
	grades:[78, 82, 79, 85],
	logIn(){
    console.log(`${this.email} has logged in`)
	},    
	logOut(){
    console.log(`${this.email} has logged out`)
},
    listGrades(){
   	console.log(`studentTwo grades are ${this.grades}`)
	}
 }

console.log(studentTwo.logIn());


function Student(name,email,grades){
	// properties
	this.name = name
	this.email = email
	this.grades = grades

	//method
	this.logIn =() => {
		console.log(`${this.email} has logged in`)
	},
	this.logOut =() => {
		console.log(`${this.email} has logged out`)
	},
	this.listGrades = () => {
		console.log(`studentTwo grades are ${this.grades}`)
	}
}

let student1 =new Student("gabby","gabby@mail.com",[1, 2, 3])
console.log(student1.logOut())


/*


*/