
/*
Quiz
What is the term given to unorganized code that's very hard to work with?
ans: Spaghetti Code

How are object literals written in JS?
ans: conts obj = {};

What do you call the concept of organizing information and functionality to belong to an object?
ans: Encapsulation

If the studentOne object has a method named enroll(), how would you invoke it?
ans: studentOne.enroll();

True or False: Objects can have objects as properties.
ans: True

What is the syntax in creating key-value pairs?
ans: key : value

True or False: A method can have no parameters and still work.
ans: True

True or False: Arrays can have objects as elements.
ans: True

True or False: Arrays are objects.
ans: True


True or False: Objects can have arrays as properties.
ans: True
*/




// Activity:

// 1. Translate the other students from our boilerplate code into their own respective objects.

function Student(name,email,grades){
	// properties
	this.name = name
	this.email = email
	this.grades = grades

	//methods
	this.logIn =() => {
		console.log(`${this.email} has logged in`)
	},
	this.logOut =() => {
		console.log(`${this.email} has logged out`)
	},
	this.listGrades = () => {
		console.log(`${this.name} grades are ${this.grades}`)
	},
	this.computeAve = () => {
		return average = this.grades.reduce((a,b)=> a + b)/ this.grades.length;
		
	},
	this.willPass = () => {
		if(this.computeAve() < 85){
			return false		
		}
		return true
	},
	this.willPassWithHonors= () => {
		if(this.computeAve() >= 90){
			return true		
		}
		if(this.computeAve() > 90){
			return false	
		}
		if(this.computeAve() > 85){
			return 	undefined	
		}
		
	} 

	
}

let studentOne = new Student("John","john@mail.com",[89, 84, 78, 88]);
let studentTwo = new Student("Joe","joe@mail.com",[78, 82, 79, 85]);
let studentThree = new Student("Jane","jane@mail.com",[87, 89, 91, 93]);
let studentFour = new Student("Jessie","jessie@mail.com",[91, 89, 92, 93]);

console.group("%cActivity 1 : Objects","color:yellow");
console.log()
console.log(studentOne);
console.log(studentTwo);
console.log(studentThree);
console.log(studentFour);
console.groupEnd()



// 2. Define a method for EACH student object that will compute for their grade average (total of grades divided by 4)


console.group("%cActivity 2 : computeAve()","color:yellow");
console.log(studentOne.computeAve());
console.log(studentTwo.computeAve());
console.log(studentThree.computeAve());
console.log(studentFour.computeAve());
console.groupEnd();

	
// 3. Define a method for all student objects named willPass() that returns a Boolean value indicating if student will pass or fail. For a student to pass, their ave. grade must be greater than or equal to 85.
console.group("%cActivity 3 : willPass()","color:yellow")
console.log(studentOne.willPass());
console.log(studentTwo.willPass());
console.log(studentThree.willPass());
console.log(studentFour.willPass());
console.groupEnd();
	

// 4. Define a method for all student objects named willPassWithHonors() that returns true if ave. grade is greater than or equal to 90, false if >= 85 but < 90, and undefined if < 85 (since student will not pass).

console.group("%cActivity 4 : willPassWithHonors()","color:yellow")
console.log(studentOne.willPassWithHonors());
console.log(studentTwo.willPassWithHonors());
console.log(studentThree.willPassWithHonors());
console.log(studentFour.willPassWithHonors());

console.groupEnd();



// 5. Create an object named classOf1A with a property named students which is an array containing all 4 student objects in it.
// const classOf1A = [studentOne, studentTwo, studentThree, studentFour];
const classOf1A = {
	students:[studentOne, studentTwo, studentThree, studentFour]
};



console.group("%cActivity 5 : classOf1A.students","color:yellow")
console.table(classOf1A.students);
console.groupEnd();




// 6. Create a method for the object classOf1A named countHonorStudents() that will return the number of honor students.

classOf1A.countHonorStudents = function(){
	let num=0;
	this.students.forEach( elem => {
		if(elem.willPassWithHonors()) num += 1;	
	})
	return num;
}

console.group("%cActivity 6 : classOf1A.countHonorStudents()","color:yellow");
console.log(classOf1A.countHonorStudents());
console.groupEnd();



// 7. Create a method for the object classOf1A named honorsPercentage() that will return the % of honor students from the batch's total number of students.

classOf1A.honorsPercentage = function() {
	return (this.countHonorStudents() / this.students.length) * 100 
}

console.group("%cActivity 7 : classOf1A.honorsPercentage()","color:yellow");
console.log(classOf1A.honorsPercentage());
console.groupEnd();




// 8. Create a method for the object classOf1A named retrieveHonorStudentInfo() that will return all honor students' emails and ave. grades as an array of objects.

classOf1A.retrieveHonorStudentInfo = function(){
	let getHonorStudent = this.students.filter( student => student.willPassWithHonors())

	return getHonorStudent.map(student => {
		return {
			email : student.email,
			aveGrade : student.computeAve()
		}
	})

}

console.group("%cActivity 8 : classOf1A.retrieveHonorStudentInfo()","color:yellow");
console.table(classOf1A.retrieveHonorStudentInfo())
console.groupEnd();



// 9. Create a method for the object classOf1A named sortHonorStudentsByGradeDesc() that will return all honor students' emails and ave. grades as an array of objects sorted in descending order based on their grade averages.

classOf1A.sortHonorStudentsByGradeDesc = function() {
	return this.retrieveHonorStudentInfo().sort((a,b) => b.aveGrade - a.aveGrade );
}

console.group("%cActivity 9 : classOf1A.sortHonorStudentsByGradeDesc()","color:yellow");
console.table(classOf1A.sortHonorStudentsByGradeDesc())
console.groupEnd();

